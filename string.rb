$KCODE = 'u'

class String
  HTML_REGEXP = /(<!--.*?--\S*>)|
                (<(?:[^"'>]*|"[^"]*"|'[^']*')+>)|
                ([^<]*)/xm

  def delete_html!
    self.replace(self.class.delete_html(self))
  end

  def self.delete_html(args)
    edit_string = ""
    args.scan(HTML_REGEXP) do |match| comment, tag, tag_data = match[0..2]
      if comment
        edit_string.concat(comment)
      elsif tag_data
        edit_string.concat(tag_data)
      end
    end
    edit_string
  end

  def self.random(length)
    chars = [('0'..'9'), ('a'..'z'), ('A'..'Z')].map { |c| c.to_a }.flatten
    (0..length-1).map { chars[rand(chars.length-1)] }.join
  end
end

#html = String.new
#html = File.read("html_tag.txt")
#p html.delete_html!
#p html

#html = File.read("html_tag.txt")
#p String.delete_html(html)

#p String.random(8)